/*
 Navicat Premium Data Transfer

 Source Server         : Maria DB Local
 Source Server Type    : MariaDB
 Source Server Version : 100407
 Source Host           : localhost:3306
 Source Schema         : db_loket

 Target Server Type    : MariaDB
 Target Server Version : 100407
 File Encoding         : 65001

 Date: 27/09/2019 12:52:57
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for m_antrian_apotek
-- ----------------------------
DROP TABLE IF EXISTS `m_antrian_apotek`;
CREATE TABLE `m_antrian_apotek`  (
  `id` int(11) NOT NULL,
  `nomor` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `tanggal` date NULL DEFAULT NULL,
  `waktu_daftar` time(0) NULL DEFAULT NULL,
  `waktu_dilayani` time(0) NULL DEFAULT NULL,
  `status` int(11) NULL DEFAULT NULL,
  `poli_id` int(11) NULL DEFAULT NULL,
  `nama` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `alamat` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for m_antrian_loket
-- ----------------------------
DROP TABLE IF EXISTS `m_antrian_loket`;
CREATE TABLE `m_antrian_loket`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomor` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `tanggal` date NULL DEFAULT NULL,
  `waktu_mulai` time(0) NULL DEFAULT NULL,
  `waktu_dilayani` time(0) NULL DEFAULT NULL,
  `status` int(11) NULL DEFAULT NULL,
  `poli_id` int(11) NULL DEFAULT NULL,
  `loket_id` int(11) NULL DEFAULT NULL,
  `call_attempt` int(11) NULL DEFAULT NULL,
  `jp_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 74 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of m_antrian_loket
-- ----------------------------
INSERT INTO `m_antrian_loket` VALUES (63, 'A001', '2019-09-26', '10:27:10', '10:50:14', 5, 1, 1, NULL, 1);
INSERT INTO `m_antrian_loket` VALUES (64, 'A002', '2019-09-26', '10:27:39', '12:00:17', 5, 2, 1, NULL, 1);
INSERT INTO `m_antrian_loket` VALUES (65, 'B001', '2019-09-26', '10:27:45', '11:01:17', 5, 2, 1, NULL, 2);
INSERT INTO `m_antrian_loket` VALUES (66, 'C001', '2019-09-26', '10:27:51', '11:04:00', 5, 3, 1, NULL, 3);
INSERT INTO `m_antrian_loket` VALUES (67, 'C002', '2019-09-26', '12:35:08', '13:43:43', 5, 1, 1, NULL, 3);
INSERT INTO `m_antrian_loket` VALUES (68, 'A003', '2019-09-26', '12:35:58', '13:28:05', 5, 1, 1, NULL, 1);
INSERT INTO `m_antrian_loket` VALUES (69, 'B002', '2019-09-26', '12:38:14', NULL, 3, NULL, 1, NULL, 2);
INSERT INTO `m_antrian_loket` VALUES (70, 'A004', '2019-09-26', '12:48:45', NULL, 1, NULL, 1, NULL, 1);
INSERT INTO `m_antrian_loket` VALUES (71, 'A005', '2019-09-26', '13:58:49', NULL, 1, NULL, 1, NULL, 1);
INSERT INTO `m_antrian_loket` VALUES (72, 'A006', '2019-09-26', '13:58:57', NULL, 1, NULL, 1, NULL, 1);
INSERT INTO `m_antrian_loket` VALUES (73, 'A007', '2019-09-26', '14:08:32', NULL, 1, NULL, 1, NULL, 1);

-- ----------------------------
-- Table structure for m_antrian_poli
-- ----------------------------
DROP TABLE IF EXISTS `m_antrian_poli`;
CREATE TABLE `m_antrian_poli`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `al_id` int(11) NULL DEFAULT NULL,
  `tanggal` date NULL DEFAULT NULL,
  `waktu_mulai` time(0) NULL DEFAULT NULL,
  `waktu_dilayani` time(0) NULL DEFAULT NULL,
  `status` int(11) NULL DEFAULT NULL,
  `nama` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `alamat` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `poli_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of m_antrian_poli
-- ----------------------------
INSERT INTO `m_antrian_poli` VALUES (1, 63, '2019-09-26', '10:50:14', NULL, 1, 'Slamet', 'Grinting, Rt 001/005', 1);
INSERT INTO `m_antrian_poli` VALUES (2, 65, '2019-09-26', '11:01:17', NULL, 1, 'Ningsih', 'Grinting', 2);
INSERT INTO `m_antrian_poli` VALUES (3, 66, '2019-09-26', '11:04:00', NULL, 1, 'Budiman', 'Grinting', 3);
INSERT INTO `m_antrian_poli` VALUES (4, 64, '2019-09-26', '12:00:17', NULL, 1, 'Paulo', 'Grinting', 2);
INSERT INTO `m_antrian_poli` VALUES (5, 68, '2019-09-26', '13:28:05', NULL, 1, 'Putra', 'Grinting', 1);
INSERT INTO `m_antrian_poli` VALUES (6, 67, '2019-09-26', '13:43:43', NULL, 3, 'Markus', 'Grinting', 1);

-- ----------------------------
-- Table structure for m_display_antrian_loket
-- ----------------------------
DROP TABLE IF EXISTS `m_display_antrian_loket`;
CREATE TABLE `m_display_antrian_loket`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `curr_no` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `a_cx` int(11) NULL DEFAULT NULL,
  `b_cx` int(11) NULL DEFAULT NULL,
  `c_cx` int(11) NULL DEFAULT NULL,
  `date` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of m_display_antrian_loket
-- ----------------------------
INSERT INTO `m_display_antrian_loket` VALUES (4, 'A004', 3, 1, 2, '2019-09-26 14:31:03');

-- ----------------------------
-- Table structure for m_jenis_pendaftaran
-- ----------------------------
DROP TABLE IF EXISTS `m_jenis_pendaftaran`;
CREATE TABLE `m_jenis_pendaftaran`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `kode` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `slug` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of m_jenis_pendaftaran
-- ----------------------------
INSERT INTO `m_jenis_pendaftaran` VALUES (1, 'Peserta BPJS', 'A', 'bpjs');
INSERT INTO `m_jenis_pendaftaran` VALUES (2, 'Peserta Umum', 'B', 'umum');
INSERT INTO `m_jenis_pendaftaran` VALUES (3, 'Lansia/Anak', 'C', 'lansia-anak');

-- ----------------------------
-- Table structure for m_loket
-- ----------------------------
DROP TABLE IF EXISTS `m_loket`;
CREATE TABLE `m_loket`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `keterangan` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `tgl_dibuat` date NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of m_loket
-- ----------------------------
INSERT INTO `m_loket` VALUES (1, 'Loket 1', 'Loket 1', '2019-09-11');
INSERT INTO `m_loket` VALUES (2, 'Loket 2', 'Loket 2', '2019-09-11');

-- ----------------------------
-- Table structure for m_nomor_pendaftaran
-- ----------------------------
DROP TABLE IF EXISTS `m_nomor_pendaftaran`;
CREATE TABLE `m_nomor_pendaftaran`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent` int(11) NULL DEFAULT NULL,
  `nomor` int(11) NULL DEFAULT NULL,
  `tanggal` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 38 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of m_nomor_pendaftaran
-- ----------------------------
INSERT INTO `m_nomor_pendaftaran` VALUES (1, 1, 9, '2019-09-06');
INSERT INTO `m_nomor_pendaftaran` VALUES (2, 2, 7, '2019-09-06');
INSERT INTO `m_nomor_pendaftaran` VALUES (3, 3, 6, '2019-09-06');
INSERT INTO `m_nomor_pendaftaran` VALUES (4, 2, 14, '2019-09-10');
INSERT INTO `m_nomor_pendaftaran` VALUES (5, 3, 15, '2019-09-10');
INSERT INTO `m_nomor_pendaftaran` VALUES (6, 1, 9, '2019-09-10');
INSERT INTO `m_nomor_pendaftaran` VALUES (7, 2, 3, '2019-09-11');
INSERT INTO `m_nomor_pendaftaran` VALUES (8, 1, 12, '2019-09-11');
INSERT INTO `m_nomor_pendaftaran` VALUES (9, 1, 19, '2019-09-14');
INSERT INTO `m_nomor_pendaftaran` VALUES (10, 2, 5, '2019-09-14');
INSERT INTO `m_nomor_pendaftaran` VALUES (11, 3, 2, '2019-09-14');
INSERT INTO `m_nomor_pendaftaran` VALUES (12, 1, 5, '2019-09-15');
INSERT INTO `m_nomor_pendaftaran` VALUES (13, 3, 3, '2019-09-15');
INSERT INTO `m_nomor_pendaftaran` VALUES (14, 2, 5, '2019-09-15');
INSERT INTO `m_nomor_pendaftaran` VALUES (15, 1, 6, '2019-09-16');
INSERT INTO `m_nomor_pendaftaran` VALUES (16, 2, 2, '2019-09-16');
INSERT INTO `m_nomor_pendaftaran` VALUES (17, 3, 7, '2019-09-16');
INSERT INTO `m_nomor_pendaftaran` VALUES (18, 1, 4, '2019-09-19');
INSERT INTO `m_nomor_pendaftaran` VALUES (19, 2, 4, '2019-09-19');
INSERT INTO `m_nomor_pendaftaran` VALUES (20, 3, 2, '2019-09-19');
INSERT INTO `m_nomor_pendaftaran` VALUES (21, 1, 2, '2019-09-20');
INSERT INTO `m_nomor_pendaftaran` VALUES (22, 3, 2, '2019-09-20');
INSERT INTO `m_nomor_pendaftaran` VALUES (23, 1, 5, '2019-09-21');
INSERT INTO `m_nomor_pendaftaran` VALUES (24, 3, 6, '2019-09-21');
INSERT INTO `m_nomor_pendaftaran` VALUES (25, 2, 4, '2019-09-21');
INSERT INTO `m_nomor_pendaftaran` VALUES (26, 1, 5, '2019-09-22');
INSERT INTO `m_nomor_pendaftaran` VALUES (27, 3, 2, '2019-09-22');
INSERT INTO `m_nomor_pendaftaran` VALUES (28, 2, 4, '2019-09-22');
INSERT INTO `m_nomor_pendaftaran` VALUES (29, 1, 5, '2019-09-23');
INSERT INTO `m_nomor_pendaftaran` VALUES (30, 2, 2, '2019-09-23');
INSERT INTO `m_nomor_pendaftaran` VALUES (31, 3, 2, '2019-09-23');
INSERT INTO `m_nomor_pendaftaran` VALUES (32, 1, 42, '2019-09-25');
INSERT INTO `m_nomor_pendaftaran` VALUES (33, 2, 5, '2019-09-25');
INSERT INTO `m_nomor_pendaftaran` VALUES (34, 3, 2, '2019-09-25');
INSERT INTO `m_nomor_pendaftaran` VALUES (35, 1, 8, '2019-09-26');
INSERT INTO `m_nomor_pendaftaran` VALUES (36, 2, 3, '2019-09-26');
INSERT INTO `m_nomor_pendaftaran` VALUES (37, 3, 3, '2019-09-26');

-- ----------------------------
-- Table structure for m_poli
-- ----------------------------
DROP TABLE IF EXISTS `m_poli`;
CREATE TABLE `m_poli`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `tgl_dibuat` date NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of m_poli
-- ----------------------------
INSERT INTO `m_poli` VALUES (1, 'Poli Umum', '2019-09-06');
INSERT INTO `m_poli` VALUES (2, 'Poli Gigi', '2019-09-06');
INSERT INTO `m_poli` VALUES (3, 'Poli BP/KIA', '2019-09-06');

-- ----------------------------
-- Table structure for m_setting
-- ----------------------------
DROP TABLE IF EXISTS `m_setting`;
CREATE TABLE `m_setting`  (
  `key` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `value` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `name` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `keterangan` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `url` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `validation` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of m_setting
-- ----------------------------
INSERT INTO `m_setting` VALUES ('app_name', 'Sistem Informasi Antrian Puskes', 'Nama Aplikasi', 'Nama Aplikasi', '-', NULL);
INSERT INTO `m_setting` VALUES ('nama_instansi', 'PUSKESMAS GRINTING', 'Nama Instansi', 'Nama Instansi', '/antrian/tiket', NULL);
INSERT INTO `m_setting` VALUES ('waktu_stop_tiket', '15:00:00', 'Waktu Stop Layanan Tiket', 'Jangan layani tiket pada jam {waktu_stop_tiket}', '/tiket', '^(?:2[0-3]|[01]?[0-9]):[0-5][0-9]:[0-5][0-9]$');
INSERT INTO `m_setting` VALUES ('running_text', 'Selamat datang di {nama_instansi}', 'Running Text', 'Text Berjalan <marquee>{running_text}<marquee>', '/antrian/tiket', NULL);
INSERT INTO `m_setting` VALUES ('tts_speak_command', '{BALCON} -n \"Vocalizer Damayanti - Indonesian For KobaSpeech 2\" -s -8 -o raw -t {text} | {LAME} --preset voice -q 9 --vbr-new - {filepath}', 'TTS Speak Command', 'Perintah keluaran suara offline', '/tts/speak/:string', NULL);
INSERT INTO `m_setting` VALUES ('tiket_digit_format', '%03d', 'Tiket Digit Format', 'Penomoran tiket, misal %04d : 0001', '/tiket', NULL);
INSERT INTO `m_setting` VALUES ('default_loket_id', '1', 'Default Loket', 'Jika Setting Loket tidak ditentukan untuk jenis pendaftaran tertentu maka gunakan ID loket ini', NULL, NULL);
INSERT INTO `m_setting` VALUES ('enable_stop_tiket', '0', 'Enable Stop Tiket', 'Jika Enable maka pelayanan tiket stop aktif', NULL, NULL);

-- ----------------------------
-- Table structure for m_setting_loket
-- ----------------------------
DROP TABLE IF EXISTS `m_setting_loket`;
CREATE TABLE `m_setting_loket`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jp_id` int(11) NULL DEFAULT NULL,
  `loket_id` int(11) NULL DEFAULT NULL,
  `tanggal` date NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for m_status_antrian
-- ----------------------------
DROP TABLE IF EXISTS `m_status_antrian`;
CREATE TABLE `m_status_antrian`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `text` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of m_status_antrian
-- ----------------------------
INSERT INTO `m_status_antrian` VALUES (1, 'queue', 'Dalam Antrian');
INSERT INTO `m_status_antrian` VALUES (2, 'on_proses', 'Sedang Dilayani');
INSERT INTO `m_status_antrian` VALUES (3, 'skip', 'Lewat');
INSERT INTO `m_status_antrian` VALUES (4, 'cancel', 'Batal');
INSERT INTO `m_status_antrian` VALUES (5, 'finish', 'Selesai');

-- ----------------------------
-- Table structure for m_user
-- ----------------------------
DROP TABLE IF EXISTS `m_user`;
CREATE TABLE `m_user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `password` varchar(225) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nama` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `tanggal` date NULL DEFAULT NULL,
  `group_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
