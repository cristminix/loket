<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Protect extends CI_Controller {

	public function index()
	{
		header('Content-Type: text/plain');
		$src_path = APPPATH . '/core/License.php'; 
		$buffer = file_get_contents($src_path);
		$buffer = php_strip_whitespace($src_path);
		
		// echo $buffer;
		$ba = string2ByteArray($buffer);

		echo json_encode($ba,TRUE);
	}
}